# SpotWelder 1-2-3 PCB

## Arduino Spot Welder Printed Circuit Board
The printed circuit board design files for the SpotWelder 1-2-3 resistance spot welder. 

The SpotWelder 1-2-3 resistance spot welder implements accurate double pulse welding control on an Arduino Pro Mini, with a simple UI based on an I2C OLED display, a rotary encoder, a push button, and a led.

See https://www.instructables.com/id/Spot-Welder-1-2-3-Arduino-Firmware/ or https://bitbucket.org/mecanicafina/swcontroller for more details.


## Files:
- schematic.pdf PDF schematic of the circuit
- SW-PCBPartsList.xlsx parts list with several links to sources 
- Archive.zip Gerber files needed to order the PCB from a fabhouse
- Other files: EagleCAD files. You don't need these unless you want to make modifications to the schematic and/or board


## Author
mecanicafina, mecanicafinam@gmail.com

## License

The PCB for the SpotWelder 1-2-3 is available under the MIT License. See the LICENSE file for more info.